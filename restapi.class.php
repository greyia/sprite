<?php
    // 2014/03/11
    // Twitter RestAPI  v1.1
    // Requires PHP 5.4.x PEAR open_ssl.dll

include_once("oauth.class.php");

class RestAPI
{
    private $OAuth;

    public function __construct($consumerKey,$consumerKeySecret,$accessToken = NULL,$accessTokenSecret = NULL)
    {
        $this->OAuth = new OAuth($consumerKey,$consumerKeySecret,$accessToken,$accessTokenSecret);
    }

}


