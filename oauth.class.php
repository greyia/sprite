<?php
	// 2014/03/10
	// OAuth Class
	// Requires PHP 5.4.x PEAR open_ssl.dll

class OAuth
{
	private $consumerKey;
	private $consumerKeySecret;
	private $accessToken;
	private $accessTokenSecret;
	private $config = [];
	public $result = [];
	
	public function __construct($consumerKey,$consumerKeySecret,$accessToken = NULL,$accessTokenSecret = NULL)
	{
		if (empty($consumerKey) || empty($consumerKeySecret)) 
		{
			throw new Exception("consumerKey || consumerKeySecret is empty"); 
		}
		
		$this->consumerKey = $consumerKey;
		$this->consumerKeySecret = $consumerKeySecret;
		$this->accessToken = $accessToken;
		$this->accessTokenSecret = $accessTokenSecret;
	}
	
	// core
	
	private function makeOAuthSign()
	{
		$OAuthSign = $this->createParamGet();
		$OAuthSign = implode('&',[$this->config['Method'], rawurlencode($this->config['URL']) , rawurlencode($OAuthSign)]);
		$OAuthKey = implode('&', [rawurlencode($this->consumerKeySecret),rawurlencode($this->accessTokenSecret)]);
		$OAuthSign = hash_hmac("sha1",$OAuthSign,$OAuthKey,true);
		$OAuthSign = base64_encode($OAuthSign);
		$OAuthSign = urlencode($OAuthSign);
		return $OAuthSign;
	}
	
	private function createParam()
	{
		$sortTarget = [];

		if (!($this->checkValidityArray()))
		{
			return false;
		}

        if (!empty($this->config['data']))
        {
            foreach (  $this->config['data'] as $key => $value)
            {
                 $value = $key ."=". rawurlencode($value);
                 $sortTarget[]  .= $value;

            }
        }

		foreach ( $this->config as $key => $value)
		{
			if ( is_array($value) && $key !== "data")
			{
				reset($value);
				$value = key($value) ."=". rawurlencode(current($value));
				$sortTarget[]  .= $value;
			}

		}
		asort($sortTarget);
        return $sortTarget;
	
	}
	
	private function createParamGet()
	{
		if (!($this->checkValidityArray()))
		{
			return false;
		}
		
		$this->result['getParam'] = implode("&", $this->createParam());
		return $this->result['getParam'];
	}
	
	private function createParamPost()
	{
		if (!($this->checkValidityArray()))
		{
			return false;
		}
	
		$this->result['postParam'] = implode(",", $this->createParam());
		return $this->result['postParam'];
	}
	
	private function readyOAuthEx($method,$url,$callbackURL = NULL)
	{
		$this->config = ['Method' => $method,
						'URL' => $url, 
						'oauth_consumer_key' => ['oauth_consumer_key' => $this->consumerKey], 
						'oauth_nonce' => ['oauth_nonce' => md5(microtime())],
						'oauth_signature_method' => ['oauth_signature_method' =>  "HMAC-SHA1"],
						'oauth_timestamp' => ['oauth_timestamp' => time()],
						'oauth_version' => ['oauth_version' =>  "1.0"]						
						];
						
		if ( $this->accessToken !== NULL)
		{
			$this->config["oauth_token"] = ["oauth_token" => $this->accessToken];		
		}

		if ( $callbackURL !== NULL)
		{
			$this->config["oauth_callback"] = ["oauth_callback" => $callbackURL];		
		}
		
	}

    private function requestOAuthPost($OAuthSign,$url, $data)
    {
       $options = [
         'http' => [
             'method' => 'POST',
             'header' =>[
                 "Authorization: $OAuthSign",
                 "Content-Type: application/x-www-form-urlencoded",
                 "Content-Length: " . strlen(http_build_query($data,"","&")),
             ],
             'content' => http_build_query($data,"","&"),
             "ignore_errors" => true
         ]
       ];

    return file_get_contents($url, false, stream_context_create($options));
    }

    private function requestOAuthGet($OAuthSign )
    {
        return file_get_contents($OAuthSign,false,stream_context_create(['http'=>["ignore_errors" => true]]));
    }

    private function checkValidityArrayOAuth($targetArray,$checkArrayKey)
	{	

		foreach ($checkArrayKey as $value)
		{
			if (empty($targetArray[$value]) )
			{	
				print "error : $value";
				return false;
			}
		
		
		}
	
	return true;
	}
	
	// private
	
	private function readyOAuthPost($url,$callbackURL = NULL)
	{
		$this->readyOAuthEx("POST",$url,$callbackURL);
	}
	
	private function readyOAuthGet($url,$callbackURL = NULL)
	{
	    $this->readyOAuthEx("GET",$url,$callbackURL);
	}

    private function getAuthRouter($method)
    {
        if ($method === "POST")
        {
            return $this->getAuthHeader();
        }

        else
        {
            return $this->getAuthURL();
        }

    }

	// interface
	
	public function checkValidityArray()
	{
		$checkArrayKey = ['Method','URL','oauth_consumer_key','oauth_nonce','oauth_signature_method','oauth_timestamp','oauth_version'];
		return $this->checkValidityArrayOAuth($this->config,$checkArrayKey);
	}

	public function setOAuth($method,$url,$callbackURL = NULL)
	{
		if ($method === "POST")
		{
			$this->readyOAuthPost($url,$callbackURL);
		}
		
		else
		{
			$this->readyOAuthGet($url,$callbackURL);
		}
		
	}

    public function setData($data)
    {
        $this->config['data'] = $data;

    }

	public function setAccessToken($accessToken = NULL,$accessTokenSecret = NULL)
	{
	
		$this->accessToken = $accessToken;
		$this->accessTokenSecret = $accessTokenSecret;
	
	}
	
	public function getAuthHeader()
	{
		if (!($this->checkValidityArray()))
		{
			return false;
		}
		
		return "OAuth ". $this->createParamPost() .",oauth_signature=". $this->makeOAuthSign();
	}
	
	public function getAuthURL()
	{
		if (!($this->checkValidityArray()))
		{
			return false;
		}
		
		return $this->config['URL'] . "?" . $this->createParamGet() . "&oauth_signature=". $this->makeOAuthSign();
	}

    public function requestOAuth($method,$OAuthSigns,$url = NULL,$data = NULL)
    {
        if ($url === NULL)
        {
            $url = $this->config['URL'];
        }
        if ($data === NULL)
        {
            $data = $this->config['data'];
        }

        if ($method === "POST")
        {
            return $this->requestOAuthPost($OAuthSigns,$url,$data);
        }

        else
        {
            return $this->requestOAuthGet($OAuthSigns);
        }


    }

	// super if

	public function OAuth($config = [])
	{
        $blankConfig = ["Method" => NULL,"URL" => NULL,"callback" => NULL,"data" => []];
        $config = array_merge ($blankConfig,$config);

        if (!is_array($config) )
        {
            return false;
        }

        if (empty($config['Method']) || empty($config['URL']) )
        {
            return false;
        }

        $this->setOAuth($config['Method'],$config['URL'],$config['callback']);

        if (!empty($config['data']))
        {
            $this->setData($config['data']);
        }

        return $this->requestOAuth($config['Method'],$this->getAuthRouter($config['Method']));

    }

}


?>
