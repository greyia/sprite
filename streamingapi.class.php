<?php
// 2014/03/11
// Twitter Streaming API  v1.1
// Requires PHP 5.4.x PEAR open_ssl.dll

include_once("oauth.class.php");

class StreamingAPI
{
    private $OAuth;

    public function __construct($consumerKey,$consumerKeySecret,$accessToken = NULL,$accessTokenSecret = NULL)
    {
    $this->OAuth = new OAuth($consumerKey,$consumerKeySecret,$accessToken,$accessTokenSecret);
    }

    public function openUserStream()
    {
        $this->OAuth->setOAuth("GET","https://userstream.twitter.com/1.1/user.json");

        $fp = fsockopen("ssl://userstream.twitter.com", 443);
        if ($fp)
        {

        fwrite($fp, "GET " . "https://userstream.twitter.com/1.1/user.json" . " HTTP/1.1\r\n"
                . "Host: userstream.twitter.com\r\n"
                . "Authorization: ". $this->OAuth->getAuthHeader() ."\r\n"
                . "\r\n");

            while (!feof($fp))
            {
                $res = fgets($fp);
                $res = json_decode($res, true);
                if (is_array($res))
                {
                    $this->onUserStream($res);
                }
            }

            fclose($fp);
        }
    }

    public function onUserStream($res)
    {
    }

}